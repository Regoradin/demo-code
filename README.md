# Code Samples

Most of my code is in private repositories now, but I have pulled out a few choice pieces to put on display here. A few loose ends have been tidied up and more extensive comments have been added to give context, but these is largely what is actually running in my games. Here is a brief rundown of what's here, and each file has a more detailed explanation of what it does in a comment at the top. Most of these are written to work in Unity, although I've tried to explain Unity-specific functionality where possible.

**Agent:** This is a class for everything that can move and fight, built for a dungeon management game. It has very basic combat, meant to be expanded in subclasses, and a flexible A* pathfinding implementation.

**PlantMeshGrower:** This class runs procedural animation for realistically growing a plant mesh between different growth stages. It is almost entirely GPU accelerated, and can handle moving tens of thousands of vertices every frame in realtime speeds.

If you would like access to more code or a look at the full project repository, please feel free to reach out to me at oliver@theabates.com.