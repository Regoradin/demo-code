using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Priority_Queue;
//priority queue borrowed from: https://github.com/BlueRaja/High-Speed-Priority-Queue-for-C-Sharp


//AGENT//
// This is the underlying class for all the adventurers, creatures, monsters, wild animals, etc. for a game set in
// a tile-based dungeon made up of many small rooms.
// This has basic functionality to run a turn system, and barebones combat functions.
// There is also an A* pathfinding implementation, to allow Agents to find their way around a room.
public class Agent : MonoBehaviour
{
    protected enum GoalType {Pass, Move, Attack, Interact }

    [Range(0, 2)]
    public float turnTime;
    
    protected GoalType currentGoalType;
    protected IInteractable interactionTarget;

    [Header("Combat")]
    public float maxHealth;
    protected float health;
    public float damage;
    protected Agent attackTarget;

    //room pathfinding -- MOVE TO FUNCTION?
    protected Vector2Int currentRoomPosition;
    protected Room currentRoom;
    protected Vector2Int currentRoomDestination;
    private List<Vector2Int> path;
    
    protected virtual void Awake()
    {
        health = maxHealth;
    }

    protected virtual void Start()
    {
        //Unity coroutines allow for running code over multiple frames of gameplay
        StartCoroutine(Turn());
    }

    private IEnumerator Turn()
    {
        while (true)
        {
            SelectGoal();
            if (currentGoalType == GoalType.Attack)
            {
                Attack(attackTarget);
            }
            if (currentGoalType == GoalType.Move)
            {
                Pathfind();
                if (path.Count == 0)
                {
                    currentGoalType = GoalType.Pass;
                }
                else
                {
                    currentRoomPosition = path[0];
                    path.RemoveAt(0);                    
                }
            }
            if (currentGoalType == GoalType.Interact)
            {
                interactionTarget.Interact(this);
            }

            //do the conversion from the room-space position of the agent to world space
            transform.position = currentRoom.RoomTileCenterWorldSpacePosition(currentRoomPosition);

            //delays the next pass through the while loop for turnTime seconds
            yield return new WaitForSeconds(turnTime);
        }
    }

    protected virtual void SelectGoal()
    {
        //Agent is the common base class for anything in the game that moves
        //Monsters, adventurers, and random wildlife all inherit from the same system
        //By overrideing SelectGoal(), they can have completely different behaviours
        //This default implementation will rarely be used, but lets me test the base Agent without having to subclass it
        currentGoalType = GoalType.Pass;
    }

    protected virtual void Attack(Agent target)
    {
        //Similar to SelectGoal(), different types of Agents can have different types of attacks and ways of taking damage/dieing,
        //and eventually tie into different animations.
        if (Utilities.isAdjacent(currentRoomPosition, attackTarget.currentRoomPosition))
        {
            Debug.Log("Attacking");
            target.Damage(damage);
        }
    }

    public virtual void Damage(float damage)
    {
        if (damage < 0)
        {
            Debug.LogWarning(name + " took negative damage");
        }
        health -= damage;
        transform.localScale = new Vector3(1f, health / maxHealth, 1f);
        
        if (health < 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        Destroy(gameObject);
    }

    private void Pathfind()
    {
        //Pathfind() does an A* search through the room, and populates path with a list of the tile coordinates in the path
        //Right now, this is actually done every time that the Agent() moves, because its goal or the room around it may change
        //Since moves happen on the scale of seconds and the rooms are fairly small, the performance hit from this isn't too bad
        SimplePriorityQueue<TileNode, float> fringe = new SimplePriorityQueue<TileNode, float>();

        //TileNode is a fairly simple object that holds temp data about a tile in the room for A*,
        // as well as calculating the weight (called "entry cost" here) and heuristic for each tile.
        //TileNodes are created uniquely for each Agent, because their entry costs may be very different.
        //For example, flying creatures might ignore rough terrain, and frightened adventurers might heavily prefer
        // to stay away from certain types of monsters.
        Dictionary<Vector2Int, TileNode> tileNodesByPosition = currentRoom.MakeTileNodesByPosition(this);
        
        //This loop attempts to find a free space near the original destination to move to, if the original destination is full
        //Because the rooms are small relative to the size and number of Agents that might be in them, this isn't strictly enforced.
        //If it can't find a valid space within 10 spaces, it can overlap other agents where necessary.

        //The actual pathfinding doesn't explicitly exclude pathing through other Agents either, because it is fairly common for
        // a group of agents moving through a corridor to completely block the corridor, which would make all pathfinding fail.
        //Instead, occupied spaces are simply weighted more heavily, and so they will be avoided if possible
        // in trying to find the least-weight path.
        int searchRadius = 1;
        while (tileNodesByPosition[currentRoomDestination].isOccupied() && searchRadius <= 10)
        {
            for (int i = -searchRadius; i < searchRadius; i++)
            {
                for (int j = -searchRadius; j < searchRadius; j++)
                {
                    Vector2Int searchPos = currentRoomDestination + new Vector2Int(i, j);
                    if (tileNodesByPosition.ContainsKey(searchPos))
                    {
                        currentRoomDestination = searchPos;
                    }
                }
            }
            searchRadius++;
        }

        //This is the actual meat of the A* algorithm
        TileNode currentTileNode = tileNodesByPosition[currentRoomPosition];
        while(currentTileNode.position != currentRoomDestination)
        {
            currentTileNode.explored = true;
            if (currentTileNode.previousTile != null) //to catch first tile
            {
                currentTileNode.totalCost = currentTileNode.previousTile.totalCost + currentTileNode.EntryCost;
            }

            foreach(Vector2Int adjacentPos in Utilities.AdjacentGridSpaces(currentTileNode.position))
            {
                if (tileNodesByPosition.ContainsKey(adjacentPos))
                {
                    //for adjacent spaces
                    TileNode adjacentNode = tileNodesByPosition[adjacentPos];
                    if (!adjacentNode.explored)
                    {
                        //get heuristic cost
                        float heuristicCost = currentTileNode.totalCost + currentTileNode.Heuristic(currentRoomDestination);
                        //if lower, add/update to fringe, and set previously
                        if (!(fringe.Contains(adjacentNode) && heuristicCost > adjacentNode.heuristicCost))
                        {
                            fringe.Enqueue(adjacentNode, heuristicCost);
                            adjacentNode.heuristicCost = heuristicCost;
                            adjacentNode.previousTile = currentTileNode;
                        }
                    }
                }
            }

            //pick next node
            currentTileNode = fringe.Dequeue();
        }

        //reconstruct path
        path = new List<Vector2Int>();
        while (currentTileNode != tileNodesByPosition[currentRoomPosition])
        {
            path.Insert(0, currentTileNode.position);
            currentTileNode = currentTileNode.previousTile;
        }
    }
}
