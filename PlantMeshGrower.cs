using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//PLANT MESH GROWER//
// This class handles procedurally animating a plant realistically growing.
// It relies on another class to give it several meshes to define the next stage of growth,
// and then uses compute shaders to efficiently animate a mesh smoothly growing into that stage

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class PlantMeshGrower : MonoBehaviour
{
    private Mesh mesh;

    public float growthTime;
    private float elapsedTime;

    private Vector3[] startVerts;
    private Vector3[] growthVectors;
    private Vector3[] targetVerts;

    public ComputeShader shader;

    private void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    private void Update()
    {
        //Every frame, the vertices of the mesh are moved slightly, to smoothly transition shapes
        //This is done in a compute shader, to allow for tens of thousands of vertices to be moved
        //each frame with manageable performance impacts.
        //The vectors that each vertex will move along are precomputed in GrowMesh() to allow this to happen efficiently
        
        if (elapsedTime >= growthTime)
        {
            mesh.vertices = targetVerts;
        }
        else if (targetVerts != null)
        {
            elapsedTime += Time.deltaTime;
            elapsedTime = Mathf.Min(elapsedTime, growthTime);

            //move vertices along growth Vectors
            ComputeBuffer vertBuffer = new ComputeBuffer(mesh.vertices.Length, 12);
            vertBuffer.SetData(mesh.vertices);
            ComputeBuffer growthVectorsBuffer = new ComputeBuffer(growthVectors.Length, 12);
            growthVectorsBuffer.SetData(growthVectors);
            
            int kernel = shader.FindKernel("MoveVertices");
            shader.SetBuffer(kernel, "vertices", vertBuffer);
            shader.SetBuffer(kernel, "movementVectors", growthVectorsBuffer);
            shader.SetFloat("movementMultiplier", Time.deltaTime / growthTime);
            shader.Dispatch(kernel, Mathf.Min(growthVectors.Length, 65535), 1, 1);

            Vector3[] newVerts = new Vector3[mesh.vertices.Length];
            vertBuffer.GetData(newVerts);
            vertBuffer.Release();
            growthVectorsBuffer.Release();

            mesh.vertices = newVerts;
        }
    }

    public void GrowMesh(Mesh freshTargetMesh, Mesh grownTargetMesh, Mesh grownOldMesh)
    {
        //GrowMesh() will grow the mesh into the given target mesh.
        //The end result of this is an array of starting positions, an array of ending positions, and an array of the vectors between them.
        //This can then be used by the Update() above to efficiently animate the growth
        
        //The difference between the "fresh" and "grown" mesh is the heart of what makes animating this difficult.

        //The "grown" mesh is pieces of the plant that already existed in the previous generation, e.g. leaves and stems that were already fully grown,
        //but may have moved into a new position based on movement lower in the plant.
        //For these, we can simply lerp the vertices between their old position and their new position.
        //This is fairly simple, assuming that grownTargetMesh and grownOldMesh have their vertices in the same order.
        //If they are slightly mismatched, the animation will be a bit funky, but the end result will still be correct.

        //The "fresh" mesh is for any new pieces of the plant, e.g. leaves that are newly grown.
        //We can't lerp these from their old position, because they have no old position.
        //The easy answer would be to start them at the origin and lerp them up to their end positions, but that would look ridiculous.
        //Instead, we set their start position to the nearest vertex in the grownOldMesh.
        //That way, the whole shape starts on something that exists, and then gets nicely grown out of there.
        //Incidentally, the core of this logic allows you to cleanly warp between any two meshes regardless of vertex count, as long as their geometry is reasonably dense,
        //which is pretty neat.

        //For a longer narrative on my plant growing system, see here: https://www.oliverabate.com/devlog/post1


        //All of this logic is somewhat obscured because it's getting routed through a couple compute shaders.
        //Running this on the GPU quickly became necessary in order to run at even slightly reasonable speeds
        
        if (freshTargetMesh.vertices.Length == 0 && grownTargetMesh.vertices.Length == 0)
        {
            return;
        }
        int kernel; //declare early, gets set for various compute shaders
        elapsedTime = 0;
        
        //calculate start for freshmesh, if they exist
        Vector3[] freshStartVerts = new Vector3[0];
        if (freshTargetMesh.vertices.Length != 0)
        {
            Vector3[] freshTargetVerts = freshTargetMesh.vertices;
            ComputeBuffer freshVertBuffer = new ComputeBuffer(freshTargetVerts.Length, 12);
            freshVertBuffer.SetData(freshTargetVerts);
        
            ComputeBuffer currentVertBuffer = new ComputeBuffer(mesh.vertices.Length, 12);
            currentVertBuffer.SetData(mesh.vertices);

            kernel = shader.FindKernel("NearestVertsOnBaseVerts");
            shader.SetBuffer(kernel, "newVerts", freshVertBuffer);
            shader.SetBuffer(kernel, "baseVerts", currentVertBuffer);
            shader.Dispatch(kernel, Mathf.Min(freshTargetVerts.Length, 65535), 1, 1);

            freshStartVerts = new Vector3[freshTargetVerts.Length];
            freshVertBuffer.GetData(freshStartVerts);

            freshVertBuffer.Release();
            currentVertBuffer.Release();
        }

        //Combine fresh and grown start vertices
        startVerts = new Vector3[freshStartVerts.Length + grownOldMesh.vertices.Length];
        freshStartVerts.CopyTo(startVerts, 0);
        grownOldMesh.vertices.CopyTo(startVerts, freshStartVerts.Length);
        
        //Combine fresh and grown target mesh into current mesh
        //Vertices will be overwritten in Update based on startVerts and growthVectors
        CombineInstance[] targetCombs = new CombineInstance[2];
        targetCombs[0].mesh = freshTargetMesh;
        targetCombs[0].transform = Matrix4x4.identity;
        targetCombs[1].mesh = grownTargetMesh;
        targetCombs[1].transform = Matrix4x4.identity;

        //set current mesh to target mesh, to get tris and normals and such
        GetComponent<MeshFilter>().mesh = new Mesh();
        //reset this to new mesh
        mesh = GetComponent<MeshFilter>().mesh;
        mesh.CombineMeshes(targetCombs);

        //store target verts for later, and set to current mesh to start verts
        targetVerts = mesh.vertices;
        mesh.vertices = startVerts;
        
        //Calculate growth vectors
        growthVectors = new Vector3[startVerts.Length];
        ComputeBuffer growthVectorBuffer = new ComputeBuffer(growthVectors.Length, 12);
        ComputeBuffer originalVertsBuffer = new ComputeBuffer(startVerts.Length, 12);
        originalVertsBuffer.SetData(startVerts);
        ComputeBuffer targetVertBuffer = new ComputeBuffer(targetVerts.Length, 12);
        targetVertBuffer.SetData(targetVerts);

        kernel = shader.FindKernel("GetGrowthVectors");
        shader.SetBuffer(kernel, "growthVectors", growthVectorBuffer);
        shader.SetBuffer(kernel, "originalVerts", originalVertsBuffer);
        shader.SetBuffer(kernel, "targetVerts", targetVertBuffer);
        shader.Dispatch(kernel, Mathf.Min(growthVectors.Length, 65535), 1, 1);

        growthVectorBuffer.GetData(growthVectors);

        growthVectorBuffer.Release();
        originalVertsBuffer.Release();
        targetVertBuffer.Release();
    }

}
